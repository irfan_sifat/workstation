<?php
if (!isset($_SESSION)) session_start();

use Carbon\Carbon;

require '../vendor/autoload.php';

function smsSend($category, $job_id)
{
  require "dbConnection.php";

  $worker_check = $mysqli->query("SELECT * FROM worker_table WHERE category_id= $category AND is_verified=1 AND is_activated=0 AND is_deleted=0");
  $send_sms = 0;
  $diff = -1;
  if ($worker_check->num_rows > 0) {
    while ($row = $worker_check->fetch_assoc()) {
      $worker_id = $row['worker_id'];
      $name = $row['first_name'];
      $recipient_no = $row['phone_number'];
      $is_sms_on = $row['is_sms_on'];
      //if ($is_sms_on == 1) {

      $is_worker_buy = $mysqli->query("SELECT id FROM worker_sms_credit WHERE worker_id = '$worker_id'");
      if ($is_worker_buy->num_rows > 0) {

        $sms_st_check =  $mysqli->query("SELECT end_date FROM worker_sms_credit WHERE worker_id = '$worker_id'");

        $time_end = $sms_st_check->fetch_assoc()['end_date'];
        $end_date = Carbon::parse($time_end, 'Asia/Dhaka');
        // $diff = var_dump($end_date->lessThanOrEqualTo($time_end));
        // $diff = $end_date->diffInDays();

        $current_date = date_create(date('Y-m-d'));
        $diff = date_diff($current_date, $end_date);
        $diff = $diff->format("%R%a");

        if ($diff >= 0) {
          $send_sms = 1;
        } else {
          $send_sms = 0;
        }

        if ($send_sms == 1) {
          $message = 'Dear ' . $name . ', a job is waiting for your response. Thanks BdWorkStation. https://bdworkstation.daanguli.com/jobDetails.php?jId=' . $job_id . '&jobStatus';
          $send = msgNotification($recipient_no, $message);
        }
      }
    }
  }
}
//}


function msgNotification($recipient_no, $message)
{
  $curl = curl_init();

  curl_setopt_array($curl, array(

    //Use SMS
    CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?mobiles=" . $recipient_no . "&authkey=260801AQvuxDmJg0so5c5303f3&route=4&sender=WorkStation&message=" . $message . "&country=0",

    //USE for OTP
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSL_VERIFYPEER => 0,
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    //echo $response;
    //    $message = 'An OTP has been sent to '.$recipient_no . $response.' Thank You.';
    //  echo "<script type='text/javascript'>alert('$message');</script>";
    //  header("Refresh:0; url=../verify.php");
    return 1;
  }
}
?>